DESCRIPTION = "Enlightenment image" 

inherit core-image 

IMAGE_FEATURES += " splash package-management "

DEVELOP = " \
  htop \
  ldd \
  tcpdump \
  openssh-ssh \
  openssh-sshd \
  openssh-scp \
  openssh-sftp-server \
  gdbserver \
  gdb \
  git \
  git-perltools \
  rsync \
"

XORG = " \
  packagegroup-core-x11 \
  xserver-xorg-extension-glx \
  xserver-xorg-extension-dri \
  xserver-xorg-utils \
  liberation-fonts \
"

IMAGE_INSTALL += " \
  ${XORG} \
  dbus \
  udev-extraconf \
  connman \
  packagegroup-enlightenment \
  terminology \
  ${DEVELOP} \
"

