SECTION = "e/libs"
LICENSE = "MIT & BSD"
DEPENDS += "pkgconfig-native"

# revision 0d93ec84b30bc1bee2caaee72d667f87bc468a70 made SRCDATE and hence PV go backwards, so we need to up PE to unbreak builds and feeds :(
PE = "2"

inherit e-base autotools

# evas-native looks at this var, so keep it

do_configure_prepend() {
    autopoint || touch config.rpath
}

do_install_prepend () {
    for i in `find ${B}/ -name "*.pc" -type f` ; do \
        sed -i -e 's:-L${STAGING_LIBDIR}:-L\$\{libdir\}:g' -e 's:-I${STAGING_LIBDIR}:-I\$\{libdir\}:g' -e 's:-I${STAGING_INCDIR}:-I\$\{includedir\}:g' $i
    done
}

