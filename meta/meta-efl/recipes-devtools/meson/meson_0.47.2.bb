HOMEPAGE = "http://mesonbuild.com"
SUMMARY = "A high performance build system"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=3b83ef96387f14655fc854ddc3c6bd57"

SRC_URI = "https://github.com/mesonbuild/meson/releases/download/${PV}/${BP}.tar.gz \
"

#file://0001-gtkdoc-fix-issues-that-arise-when-cross-compiling.patch
#file://0002-gobject-introspection-determine-g-ir-scanner-and-g-i.patch 
#file://0001-Linker-rules-move-cross_args-in-front-of-output_args.patch 
#file://0003-native_bindir.patch 


SRC_URI[md5sum] = "31bda3519d8c0eb3438267268a78085e"
SRC_URI[sha256sum] = "92d8afd921751261e36151643464efd3394162f69efbe8cd53e0a66b1cf395eb"

UPSTREAM_CHECK_URI = "https://github.com/mesonbuild/meson/releases"

inherit setuptools3

RDEPENDS_${PN} = "ninja python3-core python3-modules"

BBCLASSEXTEND = "native"

FILES_${PN} += "/usr/share/polkit-1"

