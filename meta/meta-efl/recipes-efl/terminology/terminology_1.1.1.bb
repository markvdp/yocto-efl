require ${BPN}.inc

SRC_URI = "\
    git://git.enlightenment.org/apps/terminology.git;protocol=https;tag=v${PV} \
    file://about.nls.patch \
"

S = "${WORKDIR}/git"