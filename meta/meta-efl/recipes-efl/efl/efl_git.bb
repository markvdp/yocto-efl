SUMMARY = "EFL"
LICENSE = "MIT & BSD & LGPL-2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=e5f6e713fdebf1237adf5c87de8255d8"

inherit efl gettext pkgconfig systemd meson

BBCLASSEXTEND = "native"

SRCREV = "4a4d32b60df31e25f2ad406c19324592fc69669a"
SRC_URI = "\
   git://git.enlightenment.org/core/efl.git;protocol=https;nobranch=1 \
"

S = "${WORKDIR}/git"
B = "${S}/build"

#PACKAGECONFIG_class-native ??= ""
# PACKAGECONFIG[gstreamer] = "-Dgstreamer=true,-Dgstreamer=false,gstreamer1.0"
# PACKAGECONFIG[pulseaudio] = "-D=true,-D=false,pulseaudio"
# PACKAGECONFIG[opengl-es] = "-Dopengl=es-egl,,virtual/libgles2"
#PACKAGECONFIG_class-native[examples] = "-Dbuild-examples=true,-Dbuild-examples=false,"
#PACKAGECONFIG_class-native[tests] = "-Dbuild-tests=true,-Dbuild-tests=false,libcheck"

DEPENDS = " \
  efl-native libcheck \
  systemd virtual/libiconv gettext curl glib-2.0 gnutls openssl pkgconfig \
  zlib dbus udev \
  jpeg libexif libpng tiff giflib libraw librsvg \
  libfribidi luajit poppler bullet connman \
  libsndfile1 gstreamer1.0 gstreamer1.0-plugins-base pulseaudio \
  \
  libdrm fontconfig freetype libinput libxkbcommon \
  virtual/libx11 libxext libxdamage libxrender libxcursor \
  libxcomposite libxinerama libxrandr libxtst libxscrnsaver \
  \
  util-linux libunwind\
"

DEPENDS_class-native = " \
  freetype-native gettext-native libxext-native libpng-native \
  jpeg-native giflib-native tiff-native libfribidi-native glib-2.0-native  \
  dbus-native openssl-native luajit-native libcheck-native libsndfile1-native \
"

# A stripped down native version
EXTRA_OEMESON_append_class-native = " \
  -Dsystemd=false \
  -Dgstreamer=false \
  -Daudio=false \
  -Dpulseaudio=false \
  -Davahi=false \
  -Deeze=false \
  -Dx11=false \
  -Dfontconfig=false \
  -Dopengl=none \
  -Dphysics=false \
  -Devas-loaders-disabler=webp,gst,pdf,ps,raw,svg,xcf \
  -Demotion-loaders-disabler=gstreamer,libvlc,xine,gstreamer1 \
  -Dwl-deprecated=false \
  -Ddrm-deprecated=false \
  -Dbuild-examples=false \
  -Dbuild-tests=false \
"

EXTRA_OEMESON_append_class-target = " \
  -Dopengl=none \
  -Dbuild-examples=false \
  -Dwl-deprecated=false \
  -Decore-imf-loaders-disabler=ibus,scim \
  -Devas-loaders-disabler=webp,ps \
  -Ddrm=false \
  -Ddrm-deprecated=false \
  -Dnetwork-backend=connman \
  -Dbuild-tests=false \
  -Dbuild-examples=false \
"

# because of the following error:
#    recipe-sysroot-native/usr/bin/eolian_gen: error while loading shared libraries: libeolian.so.1
# Alter LD_LIBRARY_PATH to include the native libs
do_compile_prepend () {
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${STAGING_LIBDIR_NATIVE}
}
#:${STAGING_LIBDIR_NATIVE}/../../lib

# For now have everything in this package
FILES_${PN} = "/"
