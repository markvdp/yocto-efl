require ${BPN}_1.20.inc

SRC_URI = "\
   git://git.enlightenment.org/core/efl.git;protocol=https;tag=v${PV};nobranch=1 \
"

S = "${WORKDIR}/git"
B = "${S}"

CFLAGS_prepend = "-fPIC "

