
PACKAGES =+ "edje-utils embryo-utils embryo-tests efreet-trash efreet-mime libeet libefreet \
             ecore-audio ecore-input-evas ecore-input ecore-imf-evas ecore-imf ecore-file ecore-con ecore-ipc ecore-x ecore-evas \
             libemotion eo ecore edje eet eeze efreet eina eio embryo emotion ethumb evas eldbus elua \
             elementary elementary-themes elementary-configs elementary-tests elementary-accessibility \
             evas-image-loader evas-generic-pdf-loader "

# upgrade path from 1.7
DEBIAN_NOAUTONAME_ecore-audio = "1"
DEBIAN_NOAUTONAME_ecore-input-evas = "1"
DEBIAN_NOAUTONAME_ecore-input = "1"
DEBIAN_NOAUTONAME_ecore-imf-evas = "1"
DEBIAN_NOAUTONAME_ecore-imf = "1"
DEBIAN_NOAUTONAME_ecore-file = "1"
DEBIAN_NOAUTONAME_ecore-con = "1"
DEBIAN_NOAUTONAME_ecore-ipc = "1"
DEBIAN_NOAUTONAME_ecore-x = "1"
DEBIAN_NOAUTONAME_ecore-evas = "1"
DEBIAN_NOAUTONAME_embryo = "1"

ALLOW_EMPTY_ecore = "1"
RDEPENDS_ecore = "ecore-audio ecore-input-evas ecore-input ecore-imf-evas ecore-imf ecore-file ecore-con ecore-ipc ecore-x ecore-evas"

python populate_packages_prepend () {
    for plugin_type in "engines loaders savers".split():
        bb.note( "splitting packages for evas %s..." % plugin_type )
        basedir = d.expand( '${libdir}/evas/modules/%s' % plugin_type)

        do_split_packages(d, basedir, '^(.*)',
        output_pattern = 'evas-' + plugin_type[:-1] + "-%s",
        description = 'Evas module %s',
        allow_dirs=True, recursive=False, extra_depends="" )

        plugin_type = "cserve2"
        bb.note( "splitting packages for evas %s..." % plugin_type )
        basedir = d.expand( '${libdir}/evas/%s/loaders' % plugin_type)

        do_split_packages(d, basedir, '^(.*)',
        output_pattern = 'evas-' + plugin_type + "-%s",
        description = 'Evas csever2 loader module %s',
        allow_dirs=True, recursive=False, extra_depends="" )
}

PACKAGES += "${PN}-themes ${PN}-tests ${PN}-eolian evas-loader-png \
             evas-engine-software-generic evas-engine-fb"

PACKAGES_DYNAMIC += "^evas-engine-.* ^evas-loader-.* ^evas-saver-.* ^evas-cserve2-.*"

# PACKAGES_DYNAMIC + do_split_packages isn't used for all libe libs,
# because PACKAGES_DYNAMIC would be catching too many patterns
# (or need to list all possible prefixes) and not all original packages
# were splitted like that, so it would need RCONFLICTS/RPROVIDES/RREPLACES
# combos at least in few places for upgrade path.

FILES_${PN} = " \
    ${libdir}/*.so.* \
    ${libdir}/edje/modules/${PN}/*/module.so \
    ${libdir}/${PN}/plugins/*.so \
    ${datadir}/dbus-1/services/* \
"


FILES_${PN}-themes = " \
    ${datadir}/${PN}/themes \
    ${datadir}/${PN}/data \
    ${libdir}/${PN}/plugins/data/*.edj \
    ${datadir}/${PN}/fonts \
    ${datadir}/${PN}/pointers \
    ${datadir}/${PN}/images \
    ${datadir}/${PN}/users \
    ${datadir}/${PN}/images \
    ${datadir}/${PN}/styles \
"

FILES_${PN}-dev += " \
    ${bindir}/${PN}-config \
    ${libdir}/pkgconfig/* \
    ${libdir}/lib*.la \
    ${libdir}/*.so \
    ${libdir}/${PN}/*.la \
    ${libdir}/${PN}/*/*.la \
    ${datadir}/${PN}/edje_externals \
    ${libdir}/edje/modules/${PN}/*/module.la \
"

FILES_${PN}-eolian = " \
    ${datadir}/eolian/include \
"

FILES_${PN}-staticdev += " \
    ${libdir}/${BPN}/*/*.a \
"

FILES_${PN}-dbg += " \
    ${libdir}/${PN}/.debug \
    ${libdir}/${PN}/*/.debug \
    ${libdir}/edje/modules/${PN}/*/.debug/module.so \
"

FILES_${PN}-tests = " \
    ${bindir}/${PN} \
    ${bindir}/*_* \
    ${datadir}/${PN} \
"

FILES_efreet-trash = " \
    ${libdir}/libefreet_trash${SOLIBS} \
"
FILES_efreet-mime = " \
    ${libdir}/libefreet_mime${SOLIBS} \
"
FILES_libeet = " \
    ${libdir}/libeet${SOLIBS} \
"
FILES_libefreet = " \
    ${libdir}/libefreet${SOLIBS} \
"
FILES_ecore-audio = "\
    ${libdir}/libecore_audio${SOLIBS} \
"
FILES_ecore-input-evas = "\
    ${libdir}/libecore_input_evas${SOLIBS} \
"
FILES_ecore-input = "\
    ${libdir}/libecore_input${SOLIBS} \
"
FILES_ecore-imf-evas = "\
    ${libdir}/libecore_imf_evas${SOLIBS} \
    ${libdir}/ecore-imf/modules/*/*/module.so \
"
FILES_ecore-imf = "\
    ${libdir}/libecore_imf${SOLIBS} \
"

FILES_ecore-file = "\
    ${libdir}/libecore_file${SOLIBS} \
"
FILES_ecore-con = "\
    ${libdir}/libecore_con${SOLIBS} \
    ${libdir}/ecore_con/*/*/* \
"
FILES_ecore-ipc = "\
    ${libdir}/libecore_ipc${SOLIBS} \
"
FILES_ecore-x = "\
    ${libdir}/libecore_x${SOLIBS} \
    ${libdir}/ecore_x/bin/v-*/ecore_x_vsync \
"
FILES_ecore-evas = "\
    ${libdir}/libecore_evas${SOLIBS} \
    ${libdir}/ecore-evas/engines/*/*/module.so \
"
FILES_eio = "\
    ${libdir}/libeio${SOLIBS} \
"
FILES_eina = "\
    ${libdir}/libeina${SOLIBS} \
    ${bindir}/eina-bench-cmp \
    ${bindir}/eina_modinfo \
"
FILES_edje-utils = "\
    ${bindir}/edje_* \
    ${datadir}/edje/include/edje.inc \
"
FILES_eldbus = "\
    ${libdir}/libeldbus${SOLIBS} \
"
FILES_eo = "\
    ${libdir}/libeo${SOLIBS} \
"
FILES_libemotion = "\
    ${libdir}/libemotion${SOLIBS} \
"
FILES_efreet = " \
    ${datadir}/dbus-1/services/*Efreet* \
    ${libdir}/efreet/*/efreet*create \
    ${bindir}/efreetd \
    ${datadir}/efreet \
"
FILES_eet = " \
    ${bindir}/eet \
    ${bindir}/eetpack \
    ${bindir}/vieet \
    ${bindir}/diffeet \
    ${libdir}/libeet${SOLIBS} \
"
FILES_emotion = " \
    ${datadir}/emotion \
    ${libdir}/emotion/modules/gstreamer1/*/module.so \
    ${libdir}/emotion/modules/gstreamer/*/module.so \
"
FILES_embryo-tests = " \
    ${datadir}/embryo/ \
"
FILES_embryo-utils = " \
    ${binddir}/embryo_* \
"
FILES_embryo = " \
    ${libdir}/libembryo${SOLIBS} \
"
FILES_ethumb = " \
    ${datadir}/dbus-1/services/*Ethumb* \
    ${libdir}/libethumb${SOLIBS} \
    ${libdir}/libethumb_client${SOLIBS} \
    ${bindir}/ethumbd \
    ${bindir}/ethumbd_client \
    ${bindir}/ethumb \
    ${libdir}/ethumb/*/*/*/module.so \
    ${libdir}/ethumb/*/*/*/*.edj \
    ${libdir}/ethumb_client/utils/*/ethumbd_slave \
    ${datadir}/ethumb* \
    ${libdir}/systemd/user/ethumb.service \
"

FILES_ecore = " \
    ${libdir}/libecore${SOLIBS} \
    ${libdir}/ecore*/*/*/*/module.so \
    ${datadir}/ecore* \
"

RDEPENDS_evas = "\
    evas-image-loader \
    evas-generic-pdf-loader \
"

FILES_evas = " \
    ${libdir}/libevas${SOLIBS} \
    ${libdir}/evas*/*/*/*/*/module.so \
    ${libdir}/evas*/*/*/*/*/*.edj \
    ${libdir}/evas/cserve2/bin/*/evas_cserve2* \
    ${datadir}/evas* \
    ${bindir}/evas_cserve2_* \
"

FILES_evas-image-loader = " \
    ${libdir}/evas/utils/evas_image_loader*.* \
"

FILES_evas-generic-pdf-loader = " \
    ${libdir}/evas/utils/evas_generic_pdf_loader*.* \
"

FILES_eeze = " \
    ${libdir}/libeeze${SOLIBS} \
    ${libdir}/eeze*/*/*/*/*/module.so \
    ${datadir}/eeze \
    ${bindir}/eeze_* \
"
FILES_edje = " \
    ${libdir}/libedje${SOLIBS} \
    ${libdir}/edje*/*/*/*/module.so \
    ${libdir}/edje/utils/*/epp \
    ${datadir}/edje \
    ${datadir}/mime \
"
FILES_elua = " \
    ${bindir}/elua \
    ${datadir}/elua \
"

RDEPENDS_elementary = "\
    elementary-themes \
    elementary-configs \
"

# ${libdir}/edje/modules/elm 
FILES_elementary = "\
    ${libdir}/elementary/modules/*/*/module.so \
    ${libdir}/elementary/modules/*/*/*/module.so \
    ${libdir}/elementary/modules/*/*/*.edj \
"

FILES_elementary-themes = "\
    ${datadir}/elementary/themes \
"

FILES_elementary-configs = "\
    ${datadir}/elementary/config \
"

FILES_${PN}-tests = "\
    ${bindir}/elementary* \
    ${bindir}/efl_wl_test** \
    ${datadir}/elementary/images \
    ${datadir}/elementary/objects \
    ${datadir}/elementary/examples \
    ${datadir}/elementary/edje_externals \
    ${datadir}/elementary/test* \
    ${datadir}/applications/* \
    ${datadir}/icons/* \
    ${libdir}/elementary/modules/test_entry/* \
    ${libdir}/elementary/modules/test_map/* \
"

FILES_${PN}-accessibility = "\
    ${libdir}/elementary/modules/access_output/* \
"

FILES_${PN}-dbg += " \
    ${libdir}/efreet/*/.debug \
    ${libdir}/ecore*/*/*/*/.debug \
    ${libdir}/ecore*/*/*/.debug \
    ${libdir}/evas*/*/*/*/*/.debug \
    ${libdir}/evas/*/.debug/* \
    ${libdir}/evas/cserve2/bin/*/.debug \
    ${libdir}/eeze*/*/*/*/*/.debug \
    ${libdir}/edje*/*/*/*/.debug \
    ${libdir}/edje/utils/*/.debug \
    ${libdir}/ethumb/*/*/*/.debug \
    ${libdir}/ethumb_client/utils/*/.debug \
    ${libdir}/elementary/*/*/*/.debug \
    ${libdir}/elementary/*/*/*/*/.debug/* \
    ${libdir}/elementary/*/*/.debug \
    ${libdir}/emotion/modules/gstreamer1/*/.debug/* \
    ${libdir}/emotion/modules/gstreamer/*/.debug/* \
"

FILES_${PN}-dev += " \
    ${libdir}/cmake \
    ${libdir}/ecore*/*/*/*/module.la \
    ${libdir}/evas*/*/*/*/*/module.la \
    ${libdir}/ethumb*/*/*/*/module.la \
    ${libdir}/eeze*/*/*/*/*/module.la \
    ${libdir}/edje*/*/*/*/module.la \
    ${libdir}/elementary*/*/*/*/module.la \
    ${libdir}/elementary*/*/*/*/*/module.la \
    ${libdir}/emotion/modules/gstreamer1/*/module.la \
    ${libdir}/emotion/modules/gstreamer/*/module.la \
    ${datadir}/gdb/auto-load \
    ${datadir}/eo/gdb \
    ${bindir}/eldbus-codegen \
    ${bindir}/eina_btlog \
    ${bindir}/eolian_gen \
    ${bindir}/embryo_cc \
    ${bindir}/elm_prefs_cc \
    ${bindir}/efl_debugd \
    ${bindir}/efl_debug \
    ${bindir}/ecore_evas_convert \
    ${bindir}/eo_debug \
"
