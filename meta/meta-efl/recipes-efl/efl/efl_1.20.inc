SUMMARY = "EFL"
LICENSE = "MIT & BSD & LGPL-2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=e5f6e713fdebf1237adf5c87de8255d8"

DEPENDS_class-native = " \
  freetype-native libxext-native libpng-native \
  jpeg-native tiff-native libfribidi-native glib-2.0-native  \
  dbus-native openssl-native luajit-native \
"

DEPENDS = " \
  virtual/libiconv curl glib-2.0 gnutls pkgconfig zlib jpeg openssl \
  libsndfile1 dbus libexif freetype libpng tiff  \
  fontconfig libfribidi giflib udev  \
  efl-native util-linux libunwind libxkbcommon \
  libinput libraw poppler bullet \
"

X11_DEPS = " \
  libxext virtual/libx11 libxdamage libxrender libxcursor \
  libxcomposite libxinerama libxrandr libxtst libxscrnsaver \
  librsvg \
"

inherit efl gettext pkgconfig systemd autotools-brokensep

BBCLASSEXTEND = "native"

EXTRA_OECONF_append_class-target = " \
    --disable-always-build-examples \
    --disable-spectre \
    \
    --with-edje-cc=${STAGING_BINDIR_NATIVE}/edje_cc \
    --with-eolian-gen=${STAGING_BINDIR_NATIVE}/eolian_gen \
    --with-eolian-cxx=${STAGING_BINDIR_NATIVE}/eolian_cxx \
    --with-elua=${STAGING_BINDIR_NATIVE}/elua \
    --with-eet-eet=${STAGING_BINDIR_NATIVE}/eet \
    --with-eldbus_codegen=${STAGING_BINDIR_NATIVE}/eldbus-codegen \
    --with-elementary-codegen=${STAGING_BINDIR_NATIVE}/elementary_codegen \
    --with-elm-prefs-cc=${STAGING_BINDIR_NATIVE}/elm_prefs_cc \
"

# native EFL can be as small as possible 
EXTRA_OECONF_append_class-native = " \
    --disable-always-build-examples \
    --disable-fontconfig \
    --disable-spectre \
    --disable-audio \
    --disable-physics \
    --disable-multisense \
    --disable-cserve \
    --disable-libeeze \
    --disable-poppler \
    --disable-librsvg \
    --disable-libraw  \
    --with-x11=none \
    --disable-image-loader-bmp \
    --disable-image-loader-eet \
    --disable-image-loader-generic \
    --disable-image-loader-gif \
    --disable-image-loader-ico \
    --disable-image-loader-jp2k \
    --disable-image-loader-pmaps \
    --disable-image-loader-psd \
    --disable-image-loader-tga \
    --disable-image-loader-wbmp \
    --disable-image-loader-webp \
    --disable-image-loader-xpm \
    --disable-image-loader-tgv \
    --disable-image-loader-dds \
    --enable-i-really-know-what-i-am-doing-and-that-this-will-probably-break-things-and-i-will-fix-them-myself-and-send-patches-abb \
"

PACKAGECONFIG ??= "x11 opengl-es egl gstreamer1 pulseaudio luajit systemd"

PACKAGECONFIG_class-native = "luajit"
PACKAGECONFIG[x11] = "--with-x --with-x11=xlib,--with-x11=none,${X11_DEPS}"
PACKAGECONFIG[wayland] = "--enable-wayland --enable-wayland-ivi-shell,--disable-wayland --disable-wayland-ivi-shell,wayland"
PACKAGECONFIG[drm] = "--enable-drm --enable-elput"
PACKAGECONFIG[gldrm] = "--enable-gl-drm"
PACKAGECONFIG[egl] = "--enable-egl,--disable-egl,virtual/egl"
PACKAGECONFIG[opengl-full] = "--with-opengl=full,,virtual/libgl"
PACKAGECONFIG[opengl-es] = "--with-opengl=es,,virtual/libgles2"
PACKAGECONFIG[opengl-none] = "--with-opengl=none,,"
# be aware when enabling this for efl-native, we don't provide gcc-native so you need to make
# sure that all your builders have g++ which supports -std=gnu++11
PACKAGECONFIG[c++11] = "--enable-cxx-bindings,--disable-cxx-bindings"
PACKAGECONFIG[gstreamer] = "--enable-gstreamer,--disable-gstreamer,gstreamer"
PACKAGECONFIG[gstreamer1] = "--enable-gstreamer1,--disable-gstreamer1,gstreamer1.0 gstreamer1.0-plugins-base"
PACKAGECONFIG[pulseaudio] = "--enable-pulseaudio,--disable-pulseaudio,pulseaudio"
PACKAGECONFIG[systemd] = "--enable-systemd,--disable-systemd,systemd"
PACKAGECONFIG[luajit] = "--disable-lua-old,--enable-lua-old,luajit"
PACKAGECONFIG[lua-old] = "--enable-lua-old,--disable-lua-old,lua"
PACKAGECONFIG[avahi] = "--enable-avahi,--disable-avahi,avahi"
# currently we don't provide doxygen-native
PACKAGECONFIG[doc] = "--enable-doc,--disable-doc,doxygen-native"

# Generated headerfile EFLConfig.h not found 
CFLAGS += "-I${S}/src/lib/efl"

do_install_append() {
    # don't ship precompiled lua files
    rm -f ${datadir}/elua/*/*.luac
}

# While building native, ABOUT-NLS is missing
do_configure_prepend_class-native() {
    touch ${S}/ABOUT-NLS
}

#SYSTEMD_PACKAGES = "ethumb "
#SYSTEMD_SERVICE_${PN}-boot = "ethumb.service"

# add all files into efl package
FILES_${PN} = " \
    /usr/* \
"
