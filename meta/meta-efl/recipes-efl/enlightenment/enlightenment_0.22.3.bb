DESCRIPTION = "The Enlightenment Window Manager"

require ${BPN}.inc

S = "${WORKDIR}/git"
B = "${S}"

SRC_URI = "\
    git://git.enlightenment.org/core/enlightenment.git;protocol=https;tag=v${PV};nobranch=1 \
    file://enlightenment_start.oe \
    file://applications.menu \
    file://enlightenment-module.patch \
"

