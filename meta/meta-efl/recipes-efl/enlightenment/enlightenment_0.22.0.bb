require ${BPN}.inc

S = "${WORKDIR}/git"
B = "${S}"

SRC_URI = "\
    git://git.enlightenment.org/core/enlightenment.git;protocol=https;tag=v${PV} \
    file://enlightenment_start.oe \
    file://applications.menu \
"

