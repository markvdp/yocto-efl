SUMMARY = "Library for reading and processing of RAW digicam images"
SECTION = "libs"
LICENSE = "LGPL-2.1"
LIC_FILES_CHKSUM = "file://LICENSE.LGPL;md5=68ad62c64cc6c620126241fd429e68fe"

SRC_URI = "git://github.com/LibRaw/LibRaw.git;tag=0.19.0;nobranch=1"

DEPENDS = " lcms "

S = "${WORKDIR}/git"

inherit autotools pkgconfig

EXTRA_OECONF = " \
                --enable-lcms \
                --disable-demosaic-pack-gpl2 \
                --disable-demosaic-pack-gpl3 \
                --disable-examples \
               "

