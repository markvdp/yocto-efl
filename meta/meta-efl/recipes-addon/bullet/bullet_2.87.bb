DESCRIPTION = "Bullet Physics SDK"

LICENSE = "Zlib"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=74f06ab3011994d1b43d71ecbb42a7cf"

DEPENDS = "python-native"

inherit cmake pythonnative pkgconfig
OECMAKE_GENERATOR = "Unix Makefiles"
EXTRA_OECMAKE = "-DBUILD_PYBULLET=OFF -DBUILD_PYBULLET_NUMPY=OFF -DUSE_DOUBLE_PRECISION=ON"

S = "${WORKDIR}/${PN}3-${PV}"

SRCREV = "ec444fcad918ab24e19b2e70a92712cdfd4efbbd"

SRC_URI  = " \
	 https://github.com/bulletphysics/bullet3/archive/${PV}.tar.gz \
	 file://0001-CMakeLists.txt-do-not-find-PythonLibs.patch \
	 file://bullet3_narrowing_compile_error.patch \
"

SRC_URI[md5sum] = "7566fc00d925a742e6f7ec7ba2d352de"
SRC_URI[sha256sum] = "438c151c48840fe3f902ec260d9496f8beb26dba4b17769a4a53212903935f95"

TARGET_CFLAGS += "-fPIC"
