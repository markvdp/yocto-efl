## Build

Move into a build directory and source env.sh
```bash 
. env.sh
```

to build a bootable image with xorg and enlightenment
```bash 
bitbake efl-image
```
