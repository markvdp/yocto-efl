#!/bin/bash

CURRENTDIR=$(pwd)
cd ../meta/poky/
. oe-init-build-env $CURRENTDIR
# Now we are back in the build directory again

export PROJECTREPOVERSION=$(git -C .. describe --tags --long --dirty)
export BB_ENV_EXTRAWHITE="$BB_ENV_EXTRAWHITE PROJECTREPOVERSION"

echo
echo Versions
echo Project version: $PROJECTREPOVERSION

